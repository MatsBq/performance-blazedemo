# Teste de Performance - README



Este repositório contém um script de teste de performance para avaliar a funcionalidade de compra de passagem aérea em um determinado website. O objetivo do teste é verificar se o sistema é capaz de atender ao determinado critério de aceitação de desempenho.

250 requisições por segundo com um tempo de resposta 90th percentil inferior a 2 segundos.


## Estrutura do Repositório

- `LOAD-performance-blazedemo.csv`: Arquivo contendo os resultados do teste de carga em formato CSV.
- `LOAD-performance-blazedemo.jtl`: Arquivo contendo os resultados detalhados do teste de carga em formato JTL.
- `PEAK-performance-blazedemo.csv`: Arquivo contendo os resultados do teste de pico em formato CSV.
- `PEAK-performance-blazedemo.jtl`: Arquivo contendo os resultados detalhados do teste de pico em formato JTL.
- `performance-blazedemo.jmx`: Arquivo do script de teste desenvolvido no JMeter.
- `README.md`: Este arquivo contendo informações sobre o teste de performance.

## Instruções para a execução do script

1. Instale o JMeter em sua máquina. Você pode fazer o download em [https://jmeter.apache.org/](https://jmeter.apache.org/) e seguir as instruções de instalação.

2. Faça o clone deste repositório em sua máquina local.

3. Abra o JMeter e carregue o arquivo de plano de teste `performance-blazedemo.jmx`.

4. Adicione os plugins necessários:
   - Para adicionar o plugin "Composite Timeline Graph":
     - Baixe o arquivo JAR do plugin em [https://jmeter-plugins.org/wiki/CompositeGraph/](https://jmeter-plugins.org/wiki/CompositeGraph/) e coloque-o na pasta `lib/ext` do diretório de instalação do JMeter.
     - Reinicie o JMeter para que o plugin seja carregado.
   - Para adicionar o plugin "jpgc - Standard Set":
     - Baixe o arquivo ZIP do plugin em [https://jmeter-plugins.org/?search=jpgc%20-%20Standard%20Set](https://jmeter-plugins.org/?search=jpgc%20-%20Standard%20Set) e extraia seu conteúdo.
     - Copie os arquivos JAR da pasta `lib` do plugin extraído para a pasta `lib/ext` do diretório de instalação do JMeter.
     - Reinicie o JMeter para que o plugin seja carregado.

5. Verifique e atualize as configurações do script de teste, como URLs, parâmetros de entrada e número de usuários virtuais, conforme necessário.

6. Salve as alterações e execute o plano de teste no JMeter.

## Relatório de execução dos testes

Após a conclusão do teste, você encontrará os seguintes arquivos de resultados:

- `LOAD-performance-blazedemo.jtl`: Contém os resultados do teste de carga em formato JTL.
- `PEAK-performance-blazedemo.jtl`: Contém os resultados do teste de pico em formato JTL.

## Demais considerações pertinentes ao teste

Com base na análise dos resultados, obtivemos as seguintes informações:

- Tempo de Resposta: Tivemos uma média de 434 ms (577 ms PEAK)  por chamada o que está dentro do esperado, porem, com um tempo de resposta 90th percentile de 2893 ms (3119 ms PEAK). Esses valores estão ligeiramente acima do limite aceitável de 2 segundos definido pelo critério de aceitação. Isso indica a necessidade de melhorias no desempenho do sistema para atender aos requisitos de tempo de resposta estabelecidos. Sugere-se analisar os dados e identificar possíveis gargalos ou áreas de melhoria que possam ser otimizadas para reduzir o tempo de resposta e atender ao critério de aceitação.

- Taxa de Transferência (Throughput): Tivemos uma média de 285.74 (320.63 PEAK) transações por segundo. Esse valor atende ao critério de aceitação de 250 requisições por segundo.

- Taxa de Erros: Obtivemos uma taxa de erros de 0.0992 (0.10336 PEAK), o que indica uma baixa ocorrência de falhas durante o teste.

- Outras considerações: Durante o teste, o sistema se manteve razoavelmente dentro do limiar estabelecido, com um desempenho consistente e sem interrupções significativas se adaptando a demanda. 

![Grafico LOAD](LOAD.png)

![Grafico PEAK](PEAK.png)